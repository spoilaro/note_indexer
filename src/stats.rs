use std::collections::HashMap;
use std::fs::read_to_string;
use std::{fs, path::PathBuf};

use log::{debug, info};

pub struct NoteStats<'a> {
    note_directory: &'a str,
}

impl NoteStats<'_> {
    pub fn new(note_directory: &str) -> NoteStats {
        debug!("Initializing NoteStats, dir: '{note_directory}'");
        NoteStats { note_directory }
    }

    fn get_file_names(&self) -> Result<Vec<PathBuf>, ()> {
        let entries = fs::read_dir(self.note_directory).unwrap();

        let file_names: Vec<_> = entries
            .map(|entry| {
                let path = entry.ok().unwrap().path();
                path
            })
            .collect();

        Ok(file_names)
    }

    fn read_note_lines(&self) -> Vec<String> {
        let file_names = self.get_file_names().unwrap();
        let mut all_lines: Vec<String> = vec![];

        for file_name in file_names {
            let lines: Vec<_> = read_to_string(file_name)
                .unwrap()
                .lines()
                .map(String::from)
                .filter(|l| !l.is_empty())
                .collect();

            all_lines.extend(lines);
        }

        all_lines
    }

    fn collect_tags(&self, lines: Vec<String>) -> Vec<String> {
        let tags: Vec<_> = lines
            .into_iter()
            .filter(|line| if line.starts_with("@") { true } else { false })
            .map(String::from)
            .collect();
        tags
    }

    fn aggregate_tags(&self, tags: Vec<String>) -> HashMap<String, usize> {
        let mut tag_map = HashMap::new();

        info!("Aggregating tag {} samples", tags.len());

        for tag in tags {
            match tag_map.get(&tag) {
                Some(_) => {
                    let existing_key = tag_map.get_mut(&tag).unwrap();
                    *existing_key += 1;
                }
                None => {
                    tag_map.insert(tag, 1);
                }
            }
        }

        tag_map
    }

    pub fn show_stats(&self) {
        let note_lines = self.read_note_lines();
        let tags = self.collect_tags(note_lines);

        let aggregated_tags = self.aggregate_tags(tags);

        println!("{: >8} | COUNT", "TAG");
        for t in aggregated_tags {
            println!("{: >8} | {}", t.0, t.1);
        }
    }
}
