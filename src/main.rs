use log::info;

use stats::NoteStats;
use classifier::Classifier;

mod stats;
mod classifier;

fn main() {
    env_logger::init();
    info!("Running note indexer");

    let note_directory = "examples/";
    info!("Note directory: '{}'", note_directory);

    let note_stats = NoteStats::new(note_directory);
    let classifier = Classifier::new(note_directory);

    note_stats.show_stats();
    let tag_files = classifier.generate_tag_files();


    let header = String::from("Car Test");
    let predicted = classifier.classify(header, tag_files);

    info!("Predicted: {}", predicted);

}
