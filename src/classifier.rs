use std::fs::read_to_string;
use std::usize::MAX;
use std::io::Write;
use std::{fs, path::PathBuf};
use flate2::Compression;
use flate2::write::GzEncoder;
use std::cmp::{min, max};

pub struct Classifier<'a> {
    note_directory: &'a str
}

impl Classifier<'_> {
    pub fn new(note_directory: &str) -> Classifier {
        Classifier {note_directory}
    }

    fn get_file_names(&self) -> Result<Vec<PathBuf>, ()>{
        let entries = fs::read_dir(self.note_directory).unwrap();

        let file_names: Vec<_> = entries
            .map(|entry| {
                let path = entry.ok().unwrap().path();
                path
            })
            .collect();

        Ok(file_names)
    }

    pub fn generate_tag_files(&self) -> Vec<(String, String)> {
        let file_names = self.get_file_names().unwrap();

        let tags_files = file_names.into_iter()
            .map(|file| {
                let file_string = read_to_string(file).unwrap();
                let header: String = file_string.lines().filter(|l| l.starts_with("# ")).collect();
                let tag: String = file_string.lines().filter(|l| l.starts_with("@")).collect();

                (header.replace("# ", ""), tag.replace("# ", ""))
            }).collect();

        println!("{tags_files:?}");
        tags_files

    }

    pub fn classify(&self, header: String, tag_files: Vec<(String, String)>) -> String {

        let mut min_distance: usize = MAX;
        let mut current_class = String::from("");
        let mut c = GzEncoder::new(Vec::new(), Compression::default());

        let x1 = header;
        c.write_all(x1.as_bytes()).unwrap();
        let cx1 = c.finish().unwrap().len();

        for tag_file in tag_files {
            let mut e = GzEncoder::new(Vec::new(), Compression::default());
            let x2 = tag_file.0;
            let xy = format!("{} {}", x1, x2);

            e.write_all(x2.as_bytes()).unwrap();
            let cx2 = e.finish().unwrap().len();

            let mut f = GzEncoder::new(Vec::new(), Compression::default());
            f.write_all(xy.as_bytes()).unwrap();
            let cxy = f.finish().unwrap().len();
            

            let ncd = cxy - min(cx1, cx2) / max(cx1, cx2);

            if ncd < min_distance {
                min_distance = ncd;
                current_class = tag_file.1;
            }
        }

        current_class

    }
}


